package prometheus

import (
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	prometheus_go "github.com/prometheus/client_model/go"
	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/gitlab-runner/common"
)

func TestJobStatusCollector_Collect_GroupingStatus(t *testing.T) {
	ch := make(chan prometheus.Metric, 50)

	jsc := NewJobStatusCollector()
	jsc.RecordJobStatus(common.ScriptFailure, common.Failed, "a1b2c3d4")
	jsc.RecordJobStatus(common.RunnerSystemFailure, common.Failed, "e5f67890")
	jsc.RecordJobStatus(common.NoneFailure, common.Success, "e5f67890")
	jsc.RecordJobStatus(common.NoneFailure, common.Success, "e5f67890")

	jsc.Collect(ch)
	// total 3 metrics constructed. Success metric consists of two entry counts
	assert.Len(t, ch, 3)
}

func TestJobStatusCollector_Collect_MetricsValues(t *testing.T) {
	// Test recordJobStatus metric value
	ch := make(chan prometheus.Metric, 50)

	testCases := []struct {
		name          string
		jobResult     common.JobFailureReason
		jobState      common.JobState
		expCounterVal int
		expResult     string
		runnerDes     string
	}{
		{
			name:          "scriptFailureCount==3",
			jobResult:     common.ScriptFailure,
			jobState:      common.Failed,
			runnerDes:     "a1b2c3d4",
			expCounterVal: 3,
			expResult:     "script_failure",
		},
		{
			name:          "jobSuccessCount==2",
			jobResult:     common.NoneFailure,
			jobState:      common.Success,
			runnerDes:     "a1b2c3d4",
			expCounterVal: 2,
			expResult:     "success",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			jsc := NewJobStatusCollector()
			for i := 0; i < tc.expCounterVal; i++ {
				jsc.RecordJobStatus(tc.jobResult, tc.jobState, tc.runnerDes)
			}
			jsc.Collect(ch)
			metric := &prometheus_go.Metric{}
			m := <-ch
			m.Write(metric)

			labels := make(map[string]string)
			for _, labelPair := range metric.Label {
				labels[*labelPair.Name] = *labelPair.Value
			}

			assert.Equal(t, float64(tc.expCounterVal), *metric.Counter.Value)
			assert.Equal(t, tc.expResult, labels["job_result"])
			assert.Equal(t, tc.runnerDes, labels["runner"])
		})
	}
}
