package prometheus

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/gitlab-org/gitlab-runner/common"
	"sync"
)

var numJobFailuresDesc = prometheus.NewDesc(
	"gitlab_runner_failed_jobs_total",
	"Total number of failed jobs",
	[]string{"runner", "job_result"},
	nil,
)

var numJobSuccessesDesc = prometheus.NewDesc(
	"gitlab_runner_succeeded_jobs_total",
	"Total number of succeeded jobs",
	[]string{"runner", "job_result"},
	nil,
)

type jobStatusPermutation struct {
	runnerDescription string
	jobResult         common.JobFailureReason
}

type jobStatusMetric struct {
	desc    *prometheus.Desc
	val     map[jobStatusPermutation]int64
	valType prometheus.ValueType
}

type JobStatusCollector struct {
	lock    sync.RWMutex
	metrics map[common.JobState]jobStatusMetric
}

// RecordJobStatus records job status as failed or succeeded after ci builds
func (jsc *JobStatusCollector) RecordJobStatus(reason common.JobFailureReason, jobState common.JobState, runnerDescription string) {
	normalizedReason := reason
	if normalizedReason == common.NoneFailure {
		normalizedReason = "success"
	}

	status := jobStatusPermutation{
		jobResult:         normalizedReason,
		runnerDescription: runnerDescription,
	}

	jsc.lock.Lock()
	defer jsc.lock.Unlock()

	if _, ok := jsc.metrics[jobState].val[status]; ok {
		jsc.metrics[jobState].val[status]++
	} else {
		jsc.metrics[jobState].val[status] = 1
	}
}

func (jsc *JobStatusCollector) Describe(ch chan<- *prometheus.Desc) {
	for _, metric := range jsc.metrics {
		ch <- metric.desc
	}
}

func (jsc *JobStatusCollector) Collect(ch chan<- prometheus.Metric) {
	jsc.lock.RLock()
	defer jsc.lock.RUnlock()

	for _, metric := range jsc.metrics {
		for status, number := range metric.val {
			ch <- prometheus.MustNewConstMetric(
				metric.desc,
				metric.valType,
				float64(number),
				status.runnerDescription,
				string(status.jobResult),
			)
		}
	}
}

// NewJobStatusCollector defines job status metric collector definition
func NewJobStatusCollector() *JobStatusCollector {
	return &JobStatusCollector{
		metrics: map[common.JobState]jobStatusMetric{
			common.Success: {
				desc:    numJobSuccessesDesc,
				valType: prometheus.CounterValue,
				val:     make(map[jobStatusPermutation]int64),
			},
			common.Failed: {
				desc:    numJobFailuresDesc,
				valType: prometheus.CounterValue,
				val:     make(map[jobStatusPermutation]int64),
			},
		},
	}
}
